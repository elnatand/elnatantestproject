package com.cal.playliststest.network;

import com.cal.playliststest.models.ListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;


public interface ApiService {

    @GET("youtube/playlists.json")
    Single<ListResponse> fetchPlaylistsFromServer();
}
