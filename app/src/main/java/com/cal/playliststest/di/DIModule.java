package com.cal.playliststest.di;


import com.cal.playliststest.utils.SafeHashSet;

public class DIModule {
    private static DIModule instance;

    private final SafeHashSet<DIImpl> dependencies;

    private DIModule(){
        dependencies = new SafeHashSet<>();
    }

    public static synchronized DIModule getInstance(){
        if (instance == null) {
            instance = new DIModule();
        }
        return instance;
    }

    public void registerImpl(DIImpl impl){
        impl.init();
        dependencies.add(impl);
    }

    public void clearImpls(){
        for (DIImpl impl : dependencies) {
            impl.destroy();
        }
        dependencies.clear();
    }


    public <ManagerType> ManagerType getManager(Class<ManagerType> type){
        ManagerType managerType = resolveIfExists(type);
        if (managerType == null) {
            throw new RuntimeException("Implementation for: " + type.getSimpleName() + " not registered");
        }
        return managerType;
    }

    private <ManagerType> ManagerType resolveIfExists(Class<ManagerType> type){
        for (DIImpl impl : dependencies) {
            if (type.isAssignableFrom(impl.getClass())) {
                return type.cast(impl);
            }
        }
        return null;
    }
}
