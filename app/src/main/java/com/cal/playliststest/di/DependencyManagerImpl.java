package com.cal.playliststest.di;

import com.cal.playliststest.managers.DataRepositoryManager;
import com.cal.playliststest.managers.DependencyManager;
import com.cal.playliststest.managers.NetworkManager;

public class DependencyManagerImpl extends BaseManager implements DependencyManager {
    @Override
    public void registerApplicationDependencies(){
        registerDependency(new NetworkManager());
        registerDependency(new DataRepositoryManager());
    }
}
