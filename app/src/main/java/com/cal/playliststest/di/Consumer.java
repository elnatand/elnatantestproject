package com.cal.playliststest.di;

public interface Consumer<E> {
    void accept(E e);
}