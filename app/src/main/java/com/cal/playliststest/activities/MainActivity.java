package com.cal.playliststest.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cal.playliststest.R;
import com.cal.playliststest.adapters.ExpandableAdapter;
import com.cal.playliststest.adapters.expandableAdapterComponents.Child;
import com.cal.playliststest.adapters.expandableAdapterComponents.Parent;
import com.cal.playliststest.interfaces.ChildClickListener;
import com.cal.playliststest.models.MainViewModel;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements ChildClickListener {

    private static final int REQUEST_START_STANDALONE_PLAYER = 1;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //I decide to go with MVVM architecture pattern:
        MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        //execute request for getting playlists data
        viewModel.loadPlaylists();

        //listen to response by viewModel
        viewModel.listResponse().observe(this, this::processResponse);
    }

    private void processResponse(List<Parent> parents){
        progressBar.setVisibility(View.GONE);
        if (parents == null) {
            Toast.makeText(this, R.string.error_network, Toast.LENGTH_LONG).show();
            return;
        }
        //set adapter and show playlists
        ExpandableAdapter adapter = new ExpandableAdapter(parents, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onChildClick(Child child){
        //launch standalone youtube player
        Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, getString(R.string.api_key), child.getYouTubeVideoId(), 0, true, false);
        startActivityForResult(intent, REQUEST_START_STANDALONE_PLAYER);
    }
}
