package com.cal.playliststest.interfaces;

import com.cal.playliststest.adapters.expandableAdapterComponents.Child;

public interface ChildClickListener {
    void onChildClick(Child child);
}
