package com.cal.playliststest;

import android.app.Application;

import com.cal.playliststest.utils.Core;
import com.cal.playliststest.di.DIModule;
import com.cal.playliststest.managers.DependencyManager;
import com.cal.playliststest.di.DependencyManagerImpl;


public class MyApplication extends Application implements Core {

    @Override
    public void onCreate() {
        super.onCreate();

        registerDependencies();
    }

    protected void registerDependencies(){
        DIModule.getInstance().registerImpl(new DependencyManagerImpl());
        DependencyManager dependencyManager = getManager(DependencyManager.class);
        dependencyManager.registerApplicationDependencies();
    }
}
