package com.cal.playliststest.utils;

import com.cal.playliststest.di.DIModule;

public interface Core {
    default <ManagerType> ManagerType getManager(Class<ManagerType> type){
        return DIModule.getInstance().getManager(type);
    }
}
