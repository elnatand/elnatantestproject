package com.cal.playliststest.utils;

import android.support.annotation.NonNull;

import com.cal.playliststest.di.Consumer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Lena Brusilovski on 24/02/2018.
 */

public class SafeHashSet<E> extends HashSet<E> {


    private final transient HashSet<E> set;
    private ReentrantReadWriteLock.WriteLock writeLock;

    public SafeHashSet(){
        set = new HashSet<>();
        writeLock = new ReentrantReadWriteLock().writeLock();
    }

    public SafeHashSet(Collection<? extends E> c){
        this();
        writeLock.lock();
        set.addAll(c);
        writeLock.unlock();
    }

    public SafeHashSet(int initialCapacity, float loadFactor){
        set = new HashSet<>(initialCapacity, loadFactor);
    }

    public SafeHashSet(int initialCapacity){
        set = new HashSet<>(initialCapacity);
    }

    public void forEach(@NonNull Consumer<E> action){
        for (E e : this) {
            action.accept(e);
        }
    }

    @NonNull
    public Iterator<E> iterator(){
        writeLock.lock();
        Set<E> copy = new HashSet<>(set);
        writeLock.unlock();
        return copy.iterator();
    }

    public int size(){
        return set.size();
    }

    public boolean isEmpty(){
        return set.isEmpty();
    }

    public boolean contains(Object o){
        return set.contains(o);
    }

    public boolean add(E e){
        writeLock.lock();
        boolean retVal = set.add(e);
        writeLock.unlock();
        return retVal;
    }

    public boolean remove(Object o){
        writeLock.lock();
        boolean retVal = set.remove(o);
        writeLock.unlock();
        return retVal;
    }

    public void clear(){
        writeLock.lock();
        set.clear();
        writeLock.unlock();
    }

    public Object clone(){
        super.clone();
        return set.clone();
    }
}
