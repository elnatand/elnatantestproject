package com.cal.playliststest.managers;

import com.cal.playliststest.di.BaseManager;
import com.cal.playliststest.utils.Core;
import com.cal.playliststest.models.ListResponse;

import io.reactivex.Single;

/**
 * This is the data layer. Here we can switch between data
 * from server, SQLite or any other data provider.
 */
public class DataRepositoryManager extends BaseManager implements Core {

    public Single<ListResponse> fetchPlaylistsData(){
        return getManager(NetworkManager.class).fetchPlaylistsFromServer();
    }
}
