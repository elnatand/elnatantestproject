package com.cal.playliststest.managers;

import com.cal.playliststest.di.BaseManager;
import com.cal.playliststest.models.ListResponse;
import com.cal.playliststest.network.ApiClient;
import com.cal.playliststest.network.ApiService;

import io.reactivex.Single;

public class NetworkManager extends BaseManager {

    private final ApiService apiService = ApiClient.getClient().create(ApiService.class);

    Single<ListResponse> fetchPlaylistsFromServer(){
        return apiService.fetchPlaylistsFromServer();
    }
}
