package com.cal.playliststest.managers;


import com.cal.playliststest.di.DIImpl;
import com.cal.playliststest.di.DIModule;

public interface DependencyManager {

    void registerApplicationDependencies();

    default void registerDependency(DIImpl dependency){
        DIModule.getInstance().registerImpl(dependency);
    }

    default void unregister(){
        DIModule.getInstance().clearImpls();
    }
}
