package com.cal.playliststest.models;

import java.util.Map;

public class Snippet {
    private String publishedAt;
    private String channelId;
    private String title;
    private String description;
    private Map<String, Thumbnail> thumbnails;

    public String getPublishedAt(){
        return publishedAt;
    }

    public String getChannelId(){
        return channelId;
    }

    public String getTitle(){
        return title;
    }

    public String getDescription(){
        return description;
    }

    public Map<String, Thumbnail> getThumbnails(){
        return thumbnails;
    }
}
