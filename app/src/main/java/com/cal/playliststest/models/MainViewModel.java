package com.cal.playliststest.models;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.cal.playliststest.adapters.expandableAdapterComponents.ExpandableAdapterDataTransformer;
import com.cal.playliststest.adapters.expandableAdapterComponents.Parent;
import com.cal.playliststest.utils.Core;
import com.cal.playliststest.managers.DataRepositoryManager;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class MainViewModel extends ViewModel implements Core {
    //I use "CompositeDisposable" to dispose/cancel all network observer in case the activity is no longer exist
    private final CompositeDisposable disposables = new CompositeDisposable();

    private final MutableLiveData<List<Parent>> response = new MutableLiveData<>();

    @Override
    protected void onCleared(){
        disposables.clear();
    }

    public LiveData<List<Parent>> listResponse(){
        return response;
    }

    public void loadPlaylists(){

        // Fetch data from data layer
        disposables.add(getManager(DataRepositoryManager.class).fetchPlaylistsData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ExpandableAdapterDataTransformer::mapServerDataToExpandableList)
                .subscribeWith(new DisposableSingleObserver<List<Parent>>() {
                    @Override
                    public void onSuccess(List<Parent> parents){
                        response.setValue(parents);
                    }

                    @Override
                    public void onError(Throwable e){
                        response.setValue(null);
                    }
                }));
    }
}
