package com.cal.playliststest.models;

import java.util.List;

public class ListResponse extends ItemType1 {
    private List<PlayList> items;

    public List<PlayList> getItems(){
        return items;
    }
}
