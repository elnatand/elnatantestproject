package com.cal.playliststest.models;

public class ContentDetails {
    private int itemCount;
    private String videoId;

    public int getItemCount(){
        return itemCount;
    }

    public String getVideoId(){
        return videoId;
    }
}
