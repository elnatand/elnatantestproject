package com.cal.playliststest.models;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ThumbnailType {
    public static final String DEFAULT = "default";
    public static final String MEDIUM = "medium";
    public static final String HIGH = "high";
    public static final String STANDARD = "standard";
    public static final String MAXERS = "maxres";

    // Declaration the @StringDef for these constants:
    @StringDef({DEFAULT, MEDIUM, HIGH, STANDARD, MAXERS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Thumbnail {
    }
}
