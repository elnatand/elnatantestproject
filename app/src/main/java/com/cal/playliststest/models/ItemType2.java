package com.cal.playliststest.models;

public class ItemType2 extends BaseItem  {
   private String id;
   private Snippet snippet;
   private ContentDetails contentDetails;

   public String getId(){
      return id;
   }

   public Snippet getSnippet(){
      return snippet;
   }

   public ContentDetails getContentDetails(){
      return contentDetails;
   }
}