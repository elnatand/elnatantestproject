package com.cal.playliststest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cal.playliststest.R;
import com.cal.playliststest.adapters.expandableAdapterComponents.Child;
import com.cal.playliststest.adapters.expandableAdapterComponents.ChildViewHolder;
import com.cal.playliststest.adapters.expandableAdapterComponents.Parent;
import com.cal.playliststest.adapters.expandableAdapterComponents.ParentViewHolder;
import com.cal.playliststest.interfaces.ChildClickListener;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ExpandableAdapter extends ExpandableRecyclerViewAdapter<ParentViewHolder, ChildViewHolder> {

    private final ChildClickListener childClickListener;

    public ExpandableAdapter(List<? extends ExpandableGroup> groups, ChildClickListener childClickListener){
        super(groups);
        this.childClickListener = childClickListener;
    }

    @Override
    public ParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_parent, parent, false);
        return new ParentViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_child, parent, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex){
        final Child child = ((Parent) group).getItems().get(childIndex);
        holder.bind(child, childClickListener);
    }

    @Override
    public void onBindGroupViewHolder(ParentViewHolder holder, int flatPosition, ExpandableGroup group){
        holder.setGenreTitle(group);
    }
}
