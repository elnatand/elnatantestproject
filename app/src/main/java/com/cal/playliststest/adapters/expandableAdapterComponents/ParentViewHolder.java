package com.cal.playliststest.adapters.expandableAdapterComponents;

import android.view.View;
import android.widget.TextView;

import com.cal.playliststest.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentViewHolder extends GroupViewHolder {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    public ParentViewHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setGenreTitle(ExpandableGroup group){
        tvTitle.setText(group.getTitle());
    }
}