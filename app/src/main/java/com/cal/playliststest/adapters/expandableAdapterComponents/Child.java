package com.cal.playliststest.adapters.expandableAdapterComponents;

import android.os.Parcel;
import android.os.Parcelable;

public class Child implements Parcelable {
    private String title;
    private String thumbnailUrl;
    private String youTubeVideoId;

    public String getTitle(){
        return title;
    }

    String getThumbnailUrl(){
        return thumbnailUrl;
    }

    public String getYouTubeVideoId(){
        return youTubeVideoId;
    }

    Child(String title, String thumbnailUrl, String youTubeVideoId){
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.youTubeVideoId = youTubeVideoId;
    }

    private Child(Parcel in) {
        title = in.readString();
        youTubeVideoId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(thumbnailUrl);
        dest.writeString(youTubeVideoId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Child> CREATOR = new Parcelable.Creator<Child>() {
        @Override
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        @Override
        public Child[] newArray(int size) {
            return new Child[size];
        }
    };
}