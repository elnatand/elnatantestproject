package com.cal.playliststest.adapters.expandableAdapterComponents;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cal.playliststest.R;
import com.cal.playliststest.interfaces.ChildClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChildViewHolder extends com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder {

    @BindView(R.id.ivThumbnail)
    ImageView ivThumbnail;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    public ChildViewHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Child child, ChildClickListener childClickListener){
        tvTitle.setText(child.getTitle());
        Glide.with(ivThumbnail).load(child.getThumbnailUrl()).into(ivThumbnail);
        itemView.setOnClickListener(view -> childClickListener.onChildClick(child));
    }
}
