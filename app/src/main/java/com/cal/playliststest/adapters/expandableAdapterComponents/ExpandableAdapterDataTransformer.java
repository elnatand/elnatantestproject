package com.cal.playliststest.adapters.expandableAdapterComponents;

import com.cal.playliststest.models.ListResponse;
import com.cal.playliststest.models.PlayList;
import com.cal.playliststest.models.Thumbnail;
import com.cal.playliststest.models.YouTubeItem;

import java.util.ArrayList;
import java.util.List;

import static com.cal.playliststest.models.ThumbnailType.MEDIUM;

public class ExpandableAdapterDataTransformer {

    public static List<Parent> mapServerDataToExpandableList(ListResponse listResponse){
        List<Parent> parents = new ArrayList<>();
        for (PlayList playList : listResponse.getItems()) {
            List<Child> children = transformPlaylistItemsToChildren(playList.getPlaylistItems().getItems());
            parents.add(new Parent(playList.getSnippet().getTitle(), children));
        }
        return parents;
    }

    private static List<Child> transformPlaylistItemsToChildren(List<YouTubeItem> youTubeItems){
        List<Child> children = new ArrayList<>();
        for (YouTubeItem youTubeItem : youTubeItems) {
            String url = null;

            //I preferred use const instead Enum according to this article:
            //https://android.jlelse.eu/android-performance-avoid-using-enum-on-android-326be0794dc3
            Thumbnail thumbnail = youTubeItem.getSnippet().getThumbnails().get(MEDIUM);
            if (thumbnail != null) {
                url = thumbnail.getUrl();
            }
            children.add(new Child(youTubeItem.getSnippet().getTitle(), url, youTubeItem.getContentDetails().getVideoId()));
        }
        return children;
    }
}
